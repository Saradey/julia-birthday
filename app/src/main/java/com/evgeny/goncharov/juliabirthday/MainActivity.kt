package com.evgeny.goncharov.juliabirthday

import android.content.Context
import android.content.SharedPreferences
import android.media.AudioManager
import android.os.Bundle
import android.transition.*
import android.view.View
import android.view.WindowManager
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.transition.doOnEnd
import androidx.core.transition.doOnStart
import com.evgeny.goncharov.juliabirthday.costume.ui.JuliaBirthdayView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.many_photos.*
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity(), JuliaBirthdayView.JuliaBirthdayViewCallback {

    private var density = 0.0f

    private var isShowPhoto = false

    private var animationNow = false

    companion object {
        const val NAME_SHARED = "Julia birthday"
        const val FIRST_START = "FIRST_START"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        density = applicationContext.resources.displayMetrics.density
        volumeControlStream = AudioManager.STREAM_MUSIC
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        val preferences = getSharedPreferences(NAME_SHARED, Context.MODE_PRIVATE)
        val isFistStart = preferences.getBoolean(FIRST_START, true)

        if (isFistStart) {
            GlobalScope.launch {
                grmStart.visibility = View.VISIBLE
                delay(2000)
                startShow()
            }
        } else {
            startShowPhotos()
        }
    }


    private suspend fun startShow() = withContext(Dispatchers.Main) {
        grmStart.visibility = View.GONE
        jbvShowerShow.visibility = View.VISIBLE
        jbvShowerShow.JuliaBirthdayViewBuilder()
            .setImagesRes(
                R.drawable.j0,
                R.drawable.j1,
                R.drawable.j6,
                R.drawable.j7,
                R.drawable.j8,
                R.drawable.j10,
                R.drawable.j11,
                R.drawable.j12,
                R.drawable.j13,
                R.drawable.j14,
                R.drawable.j15,
                R.drawable.j16,
                R.drawable.j17,
                R.drawable.j01,
                R.drawable.j19,
                R.drawable.j20
            )
            .setCallback(this@MainActivity)
        jbvShowerShow.startPlay()
    }


    override fun onResume() {
        super.onResume()
        jbvShowerShow.resumePlay()
    }


    override fun onPause() {
        super.onPause()
        jbvShowerShow.pausePlay()
    }


    override fun callBackToTheEnd() {
        runOnUiThread {
            GlobalScope.launch(Dispatchers.Main) {
                val sharedPreferences = getSharedPreferences(NAME_SHARED, Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putBoolean(FIRST_START, false)
                editor.apply()
                startShowPhotos()
            }
        }
    }


    private fun startShowPhotos() {
        jbvShowerShow.visibility = View.GONE
        cnlShowPhotos.visibility = View.VISIBLE
        imvPrinces.setOnClickListener {
            if (!animationNow) {
                if (isShowPhoto.not()) {
                    initShowSet(R.id.imvPrinces)
                    imvFace.visibility = View.GONE
                    imvYoung.visibility = View.GONE
                    imvVampire.visibility = View.GONE
                    imvCuteFace.visibility = View.GONE
                    imvFaceon.visibility = View.GONE
                } else {
                    initHidePrinces()
                    imvFace.visibility = View.VISIBLE
                    imvYoung.visibility = View.VISIBLE
                    imvVampire.visibility = View.VISIBLE
                    imvCuteFace.visibility = View.VISIBLE
                    imvFaceon.visibility = View.VISIBLE
                }
            }
        }
        imvFace.setOnClickListener {
            if (!animationNow) {
                if (isShowPhoto.not()) {
                    initShowSet(R.id.imvFace)
                    imvPrinces.visibility = View.GONE
                    imvYoung.visibility = View.GONE
                    imvVampire.visibility = View.GONE
                    imvCuteFace.visibility = View.GONE
                    imvFaceon.visibility = View.GONE
                } else {
                    initHideFace()
                    imvPrinces.visibility = View.VISIBLE
                    imvYoung.visibility = View.VISIBLE
                    imvVampire.visibility = View.VISIBLE
                    imvCuteFace.visibility = View.VISIBLE
                    imvFaceon.visibility = View.VISIBLE
                }
            }
        }
        imvYoung.setOnClickListener {
            if (!animationNow) {
                if (isShowPhoto.not()) {
                    initShowSet(R.id.imvYoung)
                    imvPrinces.visibility = View.GONE
                    imvFace.visibility = View.GONE
                    imvVampire.visibility = View.GONE
                    imvCuteFace.visibility = View.GONE
                    imvFaceon.visibility = View.GONE
                } else {
                    initHideYoung()
                    imvPrinces.visibility = View.VISIBLE
                    imvFace.visibility = View.VISIBLE
                    imvVampire.visibility = View.VISIBLE
                    imvCuteFace.visibility = View.VISIBLE
                    imvFaceon.visibility = View.VISIBLE
                }
            }
        }
        imvVampire.setOnClickListener {
            if (!animationNow) {
                if (isShowPhoto.not()) {
                    initShowSet(R.id.imvVampire)
                    imvPrinces.visibility = View.GONE
                    imvFace.visibility = View.GONE
                    imvYoung.visibility = View.GONE
                    imvCuteFace.visibility = View.GONE
                    imvFaceon.visibility = View.GONE
                } else {
                    initGideVampire()
                    imvPrinces.visibility = View.VISIBLE
                    imvFace.visibility = View.VISIBLE
                    imvYoung.visibility = View.VISIBLE
                    imvCuteFace.visibility = View.VISIBLE
                    imvFaceon.visibility = View.VISIBLE
                }
            }
        }
        imvCuteFace.setOnClickListener {
            if (!animationNow) {
                if (isShowPhoto.not()) {
                    initShowSet(R.id.imvCuteFace)
                    imvPrinces.visibility = View.GONE
                    imvFace.visibility = View.GONE
                    imvYoung.visibility = View.GONE
                    imvVampire.visibility = View.GONE
                    imvFaceon.visibility = View.GONE
                } else {
                    initHideCuteFace()
                    imvPrinces.visibility = View.VISIBLE
                    imvFace.visibility = View.VISIBLE
                    imvYoung.visibility = View.VISIBLE
                    imvVampire.visibility = View.VISIBLE
                    imvFaceon.visibility = View.VISIBLE
                }
            }
        }
        imvFaceon.setOnClickListener {
            if (!animationNow) {
                if (isShowPhoto.not()) {
                    initShowSet(R.id.imvFaceon)
                    imvPrinces.visibility = View.GONE
                    imvFace.visibility = View.GONE
                    imvYoung.visibility = View.GONE
                    imvVampire.visibility = View.GONE
                    imvCuteFace.visibility = View.GONE
                } else {
                    initHideFaceon()
                    imvPrinces.visibility = View.VISIBLE
                    imvFace.visibility = View.VISIBLE
                    imvYoung.visibility = View.VISIBLE
                    imvVampire.visibility = View.VISIBLE
                    imvCuteFace.visibility = View.VISIBLE
                }
            }
        }
    }


    private fun getChangeBounds(): TransitionSet {
        val transition = TransitionSet()
        transition.addTransition(Fade())
        transition.addTransition(ChangeBounds())
        transition.doOnStart {
            animationNow = true
        }
        transition.doOnEnd {
            animationNow = false
        }
        return transition
    }


    private fun initHideFaceon() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cnlShowPhotos)
        hidePhotos(
            R.id.imvFaceon,
            constraintSet,
            ConstraintSet.TOP,
            ConstraintSet.BOTTOM,
            R.id.imvVampire,
            ConstraintSet.END,
            ConstraintSet.END,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            dpFromPx(0f).toInt(),
            ConstraintSet.END,
            dpFromPx(0f).toInt(),
            -14f
        )
        TransitionManager.beginDelayedTransition(cnlShowPhotos, getChangeBounds())
        constraintSet.applyTo(cnlShowPhotos)
        isShowPhoto = false
    }


    private fun initHideCuteFace() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cnlShowPhotos)
        hidePhotos(
            R.id.imvCuteFace,
            constraintSet,
            ConstraintSet.TOP,
            ConstraintSet.BOTTOM,
            R.id.imvYoung,
            ConstraintSet.START,
            ConstraintSet.START,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            dpFromPx(0f).toInt(),
            ConstraintSet.START,
            dpFromPx(0f).toInt(),
            14f
        )
        TransitionManager.beginDelayedTransition(cnlShowPhotos, getChangeBounds())
        constraintSet.applyTo(cnlShowPhotos)
        isShowPhoto = false
    }


    private fun initGideVampire() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cnlShowPhotos)
        hidePhotos(
            R.id.imvVampire,
            constraintSet,
            ConstraintSet.TOP,
            ConstraintSet.BOTTOM,
            R.id.imvFace,
            ConstraintSet.END,
            ConstraintSet.END,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            dpFromPx(8f).toInt(),
            ConstraintSet.END,
            dpFromPx(4f).toInt(),
            16f
        )
        TransitionManager.beginDelayedTransition(cnlShowPhotos, getChangeBounds())
        constraintSet.applyTo(cnlShowPhotos)
        isShowPhoto = false
    }


    private fun initHideYoung() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cnlShowPhotos)
        hidePhotos(
            R.id.imvYoung,
            constraintSet,
            ConstraintSet.TOP,
            ConstraintSet.BOTTOM,
            R.id.imvPrinces,
            ConstraintSet.START,
            ConstraintSet.START,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            dpFromPx(20f).toInt(),
            ConstraintSet.START,
            dpFromPx(15f).toInt(),
            -8f
        )
        TransitionManager.beginDelayedTransition(cnlShowPhotos, getChangeBounds())
        constraintSet.applyTo(cnlShowPhotos)
        isShowPhoto = false
    }


    private fun initHideFace() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cnlShowPhotos)
        hidePhotos(
            R.id.imvFace,
            constraintSet,
            ConstraintSet.TOP,
            ConstraintSet.TOP,
            ConstraintSet.PARENT_ID,
            ConstraintSet.END,
            ConstraintSet.END,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            dpFromPx(40f).toInt(),
            ConstraintSet.END,
            dpFromPx(20f).toInt(),
            -10.5f
        )
        TransitionManager.beginDelayedTransition(cnlShowPhotos, getChangeBounds())
        constraintSet.applyTo(cnlShowPhotos)
        isShowPhoto = false
    }


    private fun initHidePrinces() {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cnlShowPhotos)
        hidePhotos(
            R.id.imvPrinces,
            constraintSet,
            ConstraintSet.TOP,
            ConstraintSet.TOP,
            ConstraintSet.PARENT_ID,
            ConstraintSet.START,
            ConstraintSet.START,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            dpFromPx(42f).toInt(),
            ConstraintSet.START,
            dpFromPx(20f).toInt(),
            10.5f
        )
        TransitionManager.beginDelayedTransition(cnlShowPhotos, getChangeBounds())
        constraintSet.applyTo(cnlShowPhotos)
        isShowPhoto = false
    }


    private fun initShowSet(@IdRes idImage: Int) {
        val constraintSet = ConstraintSet()
        constraintSet.clone(cnlShowPhotos)
        showPhotos(idImage, constraintSet)
        TransitionManager.beginDelayedTransition(cnlShowPhotos, getChangeBounds())
        constraintSet.applyTo(cnlShowPhotos)
        isShowPhoto = true
    }


    private fun showPhotos(@IdRes idImageView: Int, set: ConstraintSet) {
        set.clear(idImageView)
        cnlShowPhotos.setBackgroundResource(R.color.black)
        set.connect(
            idImageView,
            ConstraintSet.START,
            ConstraintSet.PARENT_ID,
            ConstraintSet.START
        )
        set.connect(idImageView, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
        set.connect(idImageView, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
        set.connect(
            idImageView,
            ConstraintSet.BOTTOM,
            ConstraintSet.PARENT_ID,
            ConstraintSet.BOTTOM
        )
        set.setRotation(idImageView, 0.0f)
        set.constrainWidth(idImageView, ConstraintSet.WRAP_CONTENT)
        set.constrainHeight(idImageView, ConstraintSet.WRAP_CONTENT)
    }


    private fun hidePhotos(
        @IdRes idImageView: Int,
        set: ConstraintSet,
        firstConnectFrom: Int,
        firstConnectTo: Int,
        firstConnectId: Int,
        secondConnectFrom: Int,
        secondConnectTo: Int,
        secondConnectId: Int,
        firstMarginKey: Int,
        firstMarginValue: Int,
        secondMarginKey: Int,
        secondMarginValue: Int,
        rotation: Float
    ) {
        set.clear(idImageView)
        cnlShowPhotos.setBackgroundResource(R.drawable.gradient_drawavle)
        set.connect(idImageView, firstConnectFrom, firstConnectId, firstConnectTo)
        set.connect(idImageView, secondConnectFrom, secondConnectId, secondConnectTo)
        set.constrainWidth(idImageView, dpFromPx(200f).toInt())
        set.constrainHeight(idImageView, dpFromPx(200f).toInt())
        set.setMargin(idImageView, firstMarginKey, firstMarginValue)
        set.setMargin(idImageView, secondMarginKey, secondMarginValue)
        set.setRotation(idImageView, rotation)
    }


    private fun dpFromPx(dp: Float): Float {
        return dp * density
    }

    override fun onBackPressed() {}


}
