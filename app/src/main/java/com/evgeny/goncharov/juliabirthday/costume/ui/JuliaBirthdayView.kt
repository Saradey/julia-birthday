package com.evgeny.goncharov.juliabirthday.costume.ui

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.util.AttributeSet
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.RawRes
import androidx.core.content.res.getResourceIdOrThrow
import androidx.core.graphics.scale
import com.evgeny.goncharov.juliabirthday.R
import java.util.*


/**
 * Жрет фотографии
 * Жрет музыку
 * И все это показывает с заданным интервалом времени проигрывая музыку
 * Недоступен портретный режим
 */

class JuliaBirthdayView : View {


    constructor(context: Context, attr: AttributeSet) : super(context, attr) {
        val typeAdapter = context.obtainStyledAttributes(attr, R.styleable.JuliaBirthdayView)
        toTheEndOfTheMusic =
            typeAdapter.getBoolean(R.styleable.JuliaBirthdayView_to_the_end_of_the_music, false)
        howMatchSecondsPlay =
            typeAdapter.getInt(R.styleable.JuliaBirthdayView_how_match_seconds_play, 0)
        pauseAvailable =
            typeAdapter.getBoolean(R.styleable.JuliaBirthdayView_pause_available, false)
        musicResId = typeAdapter.getResourceIdOrThrow(R.styleable.JuliaBirthdayView_music_id_res)
        mediaPlayer = MediaPlayer.create(context, musicResId)
        howMatchSecondsPlay = mediaPlayer.duration
        replayImage = typeAdapter.getBoolean(R.styleable.JuliaBirthdayView_replay_image, false)
        interval = typeAdapter.getInteger(R.styleable.JuliaBirthdayView_interval, 0)
        pauseInterval = typeAdapter.getInteger(R.styleable.JuliaBirthdayView_pause_interval, 0)
        typeAdapter.recycle()
    }


    constructor(context: Context) : super(context) {
    }

    private val builder = JuliaBirthdayViewBuilder()

    //проигрывание музыки
    private lateinit var mediaPlayer: MediaPlayer

    //проигрывающиеся изображение по порядку
    private val listImageDrawable = mutableListOf<Int>()

    //проигрываться до конца музыки или нет
    private var toTheEndOfTheMusic = false

    //сколько секунд проигрываться
    private var howMatchSecondsPlay = 0

    //доступна ли пауза
    private var pauseAvailable = false

    //проигрывать заново изображения, если они закончились, если false, проигрывание завершится
    private var replayImage = false

    private var interval = 0

    private var endInterval = 0

    private var musicResId = 0

    private val mainMatrix = Matrix()

    private var bitmapSource: Bitmap? = null

    //какая анимация сейчас проигрывается
    private var howPlayAnimationNow = -1

    //какое изображение по своей очереди
    private var indexBitmapPlayNow = 0

    private var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var timerInterval = Timer()

    private var callback: JuliaBirthdayViewCallback? = null

    //пауза после проигрывания анимации до следующего проигрывания
    private var pauseInterval = 0

    private var mainWith = 0
    private var mainHeight = 0

    private var animator: ValueAnimator = ValueAnimator()

    private var pauseMusicRightNow = false

    inner class JuliaBirthdayViewBuilder {

        /**
         * @param idImage будет отображаться по очередности первого засеченого элемента
         */
        fun setImageRes(@DrawableRes idImage: Int): JuliaBirthdayViewBuilder {
            listImageDrawable.add(idImage)
            return this
        }

        /**
         * @param idImage: Int будет отображаться по очередности первого засеченого элемента аналогично:
         * @param idImage
         */
        fun setImagesRes(vararg idImage: Int): JuliaBirthdayViewBuilder {
            listImageDrawable.addAll(idImage.toList())
            return this
        }

        /**
         * @param resMusicId музыка нас связала
         */
        fun setMusicResource(@RawRes resMusicId: Int): JuliaBirthdayViewBuilder {
            mediaPlayer = MediaPlayer.create(context, resMusicId)
            mediaPlayer.setScreenOnWhilePlaying(true)
            if (toTheEndOfTheMusic) {
                howMatchSecondsPlay = mediaPlayer.duration
            }
            return this
        }


        fun setCallback(callback: JuliaBirthdayViewCallback): JuliaBirthdayViewBuilder {
            this@JuliaBirthdayView.callback = callback
            return this
        }
    }


    /**
     * стартуем
     */
    fun startPlay() {
        if (toTheEndOfTheMusic) {
            val teampValue = (howMatchSecondsPlay / 1000) / listImageDrawable.size
            endInterval = (howMatchSecondsPlay / 1000) % listImageDrawable.size
            pauseInterval = (teampValue * 10) / 30
            interval = teampValue - pauseInterval
        }
        mediaPlayer.start()
        timerInterval.schedule(object : TimerTask() {
            override fun run() {
                if (indexBitmapPlayNow < listImageDrawable.size) {
                    howPlayAnimationNow = (Math.random() * 5).toInt()
                    when (howPlayAnimationNow) {
                        EXITS_FROM_LEFT_EDGE -> playExistsFromLeftEdge()
                        EXITS_FROM_RIGHT_EDGE -> playExistsFromRight()
                        EXITS_FROM_TOP_EDGE -> playExistsFromTop()
                        EXITS_FROM_BOTTOM_EDGE -> playExistsFromBottom()
                        EXITS_FROM_SCALE_TOP_LEFT_ANGLE -> playExistsRotateAndScaleFromCenter()
                    }
                    indexBitmapPlayNow++
                } else {
                    timerInterval.cancel()
                    Thread.sleep(endInterval.toLong() * 1000 + 2000)
                    callback?.callBackToTheEnd()
                }
            }
        }, 0, (interval.toLong() + pauseInterval) * 1000)
    }


    /**
     * ставим на паузу
     */
    fun pausePlay() {
        if (mediaPlayer.isPlaying) {
            pauseMusicRightNow = true
            mediaPlayer.pause()
        }
        if (animator.isRunning) {
            animator.pause()
        }
    }


    /**
     * резюмим музыку
     */
    fun resumePlay() {
        if (pauseMusicRightNow) {
            pauseMusicRightNow = false
            mediaPlayer.start()
        }
        if (animator.isPaused) {
            animator.resume()
        }
    }


    override fun onDraw(canvas: Canvas?) {
        if (animator.isRunning || indexBitmapPlayNow == listImageDrawable.size) {
            bitmapSource?.let {
                canvas?.drawBitmap(it, mainMatrix, paint)
            }
        }
    }


    private fun playExistsRotateAndScaleFromCenter() {
        prepareBitmap()
        animator = ValueAnimator.ofFloat(
            0f,
            1f
        )
        animator.duration = interval.toLong() * 1000

        animator.addUpdateListener {
            mainMatrix.setTranslate(
                (mainWith - bitmapSource?.width!!).toFloat(),
                (mainHeight / 2).toFloat() - (bitmapSource?.height!! / 2).toFloat()
            )
            mainMatrix.postScale(
                it.animatedValue as Float,
                it.animatedValue as Float
            )
            invalidate()
        }
        post {
            animator.start()
        }
    }


    private fun playExistsFromBottom() {
        prepareBitmap()
        animator =
            ValueAnimator.ofFloat(
                mainHeight.toFloat(),
                (mainHeight / 2 - bitmapSource?.height!! / 2).toFloat()
            )
        animator.duration = interval.toLong() * 1000
        animator.addUpdateListener {
            mainMatrix.setTranslate(
                mainWith.minus(bitmapSource?.width!!).toFloat(),
                (it.animatedValue as Float)
            )
            invalidate()
        }
        post {
            animator.start()
        }
    }


    private fun playExistsFromTop() {
        prepareBitmap()
        animator =
            ValueAnimator.ofFloat(
                (-bitmapSource?.height?.toFloat()!!),
                (mainHeight / 2 - bitmapSource?.height!! / 2).toFloat()
            )
        animator.duration = interval.toLong() * 1000
        animator.addUpdateListener {
            mainMatrix.setTranslate(
                mainWith.minus(bitmapSource?.width!!).toFloat(),
                (it.animatedValue as Float)
            )
            invalidate()
        }

        post {
            animator.start()
        }
    }


    private fun playExistsFromLeftEdge() {
        prepareBitmap()
        animator = ValueAnimator.ofFloat(
            (-bitmapSource?.width?.toFloat()!!),
            mainWith.minus(bitmapSource?.width!!).toFloat()
        )
        animator.duration = interval.toLong() * 1000

        animator.addUpdateListener {
            mainMatrix.setTranslate(
                (it.animatedValue as Float),
                (mainHeight / 2 - bitmapSource?.height!! / 2).toFloat()
            )
            invalidate()
        }
        post {
            animator.start()
        }
    }


    private fun playExistsFromRight() {
        prepareBitmap()
        animator = ValueAnimator.ofFloat(
            bitmapSource?.width?.toFloat()!!,
            mainWith.minus(bitmapSource?.width!!).toFloat()
        )
        animator.duration = interval.toLong() * 1000

        animator.addUpdateListener {
            mainMatrix.setTranslate(
                (it.animatedValue as Float),
                (mainHeight / 2 - bitmapSource?.height!! / 2).toFloat()
            )
            invalidate()
        }
        post {
            animator.start()
        }
    }


    private fun prepareBitmap() {
        mainMatrix.reset()
        if (bitmapSource?.isRecycled?.not() == true) {
            bitmapSource?.recycle()
        }
        bitmapSource = BitmapFactory.decodeResource(
            resources,
            listImageDrawable[indexBitmapPlayNow]
        )
        if (bitmapSource?.width!! > mainWith) {
            val scale = bitmapSource?.width!! / mainWith
            bitmapSource = bitmapSource?.scale(
                bitmapSource?.width!! / scale,
                bitmapSource?.height!! / scale
            )
        }
    }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mainWith = w
        mainHeight = h
    }


    interface JuliaBirthdayViewCallback {
        fun callBackToTheEnd()
    }


    companion object {
        //выезжает из левого края
        const val EXITS_FROM_LEFT_EDGE = 0

        //выезжает из правого края
        const val EXITS_FROM_RIGHT_EDGE = 1

        //выезжает сверху
        const val EXITS_FROM_TOP_EDGE = 2

        //выезжает снизу
        const val EXITS_FROM_BOTTOM_EDGE = 3

        //появляется из поворота и увелечения из центра
        const val EXITS_FROM_SCALE_TOP_LEFT_ANGLE = 4
    }


}